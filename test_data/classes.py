class AsyncEngine:
    pass


class AsyncSession:
    pass


class async_sessionmaker:
    def __call__(self, *args, **kwargs):
        pass


def get_engine() -> AsyncEngine:
    pass


def get_session_factory(engine: AsyncEngine) -> async_sessionmaker:
    pass


class ConnectionManager:
    @classmethod
    def get_instance(cls) -> "ConnectionManager":
        pass


class Repository:
    def __init__(self, session: AsyncSession, **kwargs):
        pass


class HostRepository(Repository):
    pass


class HostService:
    pass


class CustomerRepository(Repository):
    pass


class CustomerService:
    pass


class ProjectRepository(Repository):
    pass


class ProjectService:
    pass


class PermissionRepository(Repository):
    pass


class PermissionService:
    pass


class ApiKeyRepository(Repository):
    pass


class ApiKeyService:
    pass


class UserRepository(Repository):
    pass


class UserService:
    pass


class SecurityService:
    def __init__(self, api_key_service: ApiKeyService, user_service: UserService):
        pass


class HostRepository(Repository):
    pass


class HostService:
    pass
