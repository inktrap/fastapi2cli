import pytest
from fastapi.routing import APIRoute
from httpx import Response
from pydantic import BaseModel

from fastapi2cli.web.authentication import AuthenticationManager
from fastapi2cli.web.server_interactor import ServerInteractor, RequestExecutor

MODULE_PACKAGE = "fastapi2cli.web.server_interactor"


@pytest.fixture
def server_interactor(mocker):
    return ServerInteractor("http://test-server", mocker.Mock(spec=RequestExecutor),
                            mocker.Mock(spec=AuthenticationManager))


def test_get_response(mocker, server_interactor):
    # Mocking external functions
    get_formatted_url_patch = mocker.patch(f"{MODULE_PACKAGE}.get_formatted_url")
    get_body_patch = mocker.patch(f"{MODULE_PACKAGE}.get_body")
    get_query_parameters_patch = mocker.patch(f"{MODULE_PACKAGE}.get_query_parameters")
    get_header_parameters_patch = mocker.patch(f"{MODULE_PACKAGE}.get_header_parameters")
    get_cookie_parameters_patch = mocker.patch(f"{MODULE_PACKAGE}.get_cookie_parameters")
    server_interactor.authentication_manager.is_route_authenticated.return_value = False

    # Test parameters
    route = APIRoute("/test", lambda: None)
    http_parameters = {"param": "value"}

    # Call the method
    server_interactor.get_response(route, **http_parameters)

    # Assertions
    get_formatted_url_patch.assert_called_once_with("http://test-server", route, **http_parameters)
    get_body_patch.assert_called_once_with(route, **http_parameters)
    get_query_parameters_patch.assert_called_once_with(route, **http_parameters)
    get_header_parameters_patch.assert_called_once_with(route, **http_parameters)
    get_cookie_parameters_patch.assert_called_once_with(route, **http_parameters)

    # Check request_executor.request method call
    server_interactor.request_executor.request.assert_called_once_with(
        "GET",
        get_formatted_url_patch.return_value,
        params=get_query_parameters_patch.return_value,
        headers=get_header_parameters_patch.return_value,
        cookies=get_cookie_parameters_patch.return_value,
        json=get_body_patch.return_value.model_dump.return_value
    )


def test_get_response_authenticated(mocker, server_interactor):
    # Mocking external functions
    get_formatted_url_patch = mocker.patch(f"{MODULE_PACKAGE}.get_formatted_url")
    get_body_patch = mocker.patch(f"{MODULE_PACKAGE}.get_body")
    get_query_parameters_patch = mocker.patch(f"{MODULE_PACKAGE}.get_query_parameters", return_value={"q": "u"})
    get_header_parameters_patch = mocker.patch(f"{MODULE_PACKAGE}.get_header_parameters", return_value={"h": "e"})
    get_cookie_parameters_patch = mocker.patch(f"{MODULE_PACKAGE}.get_cookie_parameters", return_value={"c": "o"})
    server_interactor.authentication_manager.is_route_authenticated.return_value = True
    server_interactor.authentication_manager.headers = {"Authorization": "Bearer token"}
    server_interactor.authentication_manager.query_parameters = {"api_key": "auth_param"}
    server_interactor.authentication_manager.cookies = {"session_id": "auth_session_id"}

    # Test parameters
    route = APIRoute("/test", lambda: None)
    http_parameters = {"param": "value"}

    # Call the method
    server_interactor.get_response(route, **http_parameters)

    # Assertions
    get_formatted_url_patch.assert_called_once_with("http://test-server", route, **http_parameters)
    get_body_patch.assert_called_once_with(route, **http_parameters)
    get_query_parameters_patch.assert_called_once_with(route, **http_parameters)
    get_header_parameters_patch.assert_called_once_with(route, **http_parameters)
    get_cookie_parameters_patch.assert_called_once_with(route, **http_parameters)

    # Check request_executor.request method call
    server_interactor.request_executor.request.assert_called_once_with(
        "GET",
        get_formatted_url_patch.return_value,
        params={"api_key": "auth_param", "q": "u"},
        headers={"Authorization": "Bearer token", "h": "e"},
        cookies={"session_id": "auth_session_id", "c": "o"},
        json=get_body_patch.return_value.model_dump.return_value
    )


def test_request_no_response_model(server_interactor, mocker):
    # Mock get_response method to return a Response object
    response = mocker.Mock(spec=Response, status_code=200)
    response.json.return_value = {"key": "value"}
    get_response_mock = mocker.patch.object(ServerInteractor, 'get_response', return_value=response)

    # Create a route with no response model
    route = APIRoute("/", lambda: None, response_model=None)

    # Call the request method
    response = server_interactor.request(route)

    # Assert that get_response method was called
    get_response_mock.assert_called_once_with(route)

    # Assert the response
    assert response == {"key": "value"}


def test_request_single_response_model(server_interactor, mocker):
    class TestModel(BaseModel):
        key: str

    # Mock get_response method to return a Response object
    response = mocker.Mock(spec=Response, status_code=200)
    response.json.return_value = {"key": "value"}
    get_response_mock = mocker.patch.object(ServerInteractor, 'get_response', return_value=response)

    # Create a route with a single response model
    route = APIRoute("/", lambda: None, response_model=TestModel)

    # Call the request method
    response = server_interactor.request(route)

    # Assert that get_response method was called
    get_response_mock.assert_called_once_with(route)

    # Assert the response
    assert response == TestModel(key="value")


def test_request_list_response_model(server_interactor, mocker):
    class TestModel(BaseModel):
        key: str

    def fun():
        pass

    # Mock get_response method to return a Response object
    response = mocker.Mock(spec=Response, status_code=200)
    response.json.return_value = [{"key": "value"}]
    get_response_mock = mocker.patch.object(ServerInteractor, 'get_response', return_value=response)

    # Create a route with a list response model
    route = APIRoute("/", fun, response_model=list[TestModel])

    # Call the request method
    response = server_interactor.request(route)

    # Assert that get_response method was called
    get_response_mock.assert_called_once_with(route)

    # Assert the response
    assert response == [TestModel(key="value")]


def test_request_with_unknown_base_model(server_interactor, mocker):
    # Mock get_response method to return a Response object
    response = mocker.Mock(spec=Response, status_code=200)
    response.json.return_value = {"key": "value"}
    get_response_mock = mocker.patch.object(ServerInteractor, 'get_response', return_value=response)

    def fun():
        pass

    # Create a route with a response model that is not a BaseModel
    route = APIRoute("/", lambda: None, response_model=int)

    # Call the request method and expect a ValueError
    with pytest.raises(ValueError) as exc_info:
        server_interactor.request(route)

    # Assert that get_response method was called
    assert exc_info.value.args == (int,)
    get_response_mock.assert_called_once_with(route)
