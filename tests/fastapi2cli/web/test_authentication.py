from unittest.mock import MagicMock

import pytest
from fastapi.params import Security

from fastapi2cli.web.authentication import (
    AuthenticationResolverBase, NoAuthResolver, AuthenticationManager, NoAuthResolverException
)


class MockAuthenticationResolver(AuthenticationResolverBase):
    server_url: str = None

    def get_headers(self):
        return {'Authorization': 'Bearer my_token'}

    def get_cookies(self):
        return {'session_id': 'my_session_id'}

    def get_query_parameters(self):
        return {'api_key': 'my_api_key'}

    def set_server_url(self, server_url: str):
        self.server_url = server_url


class TestNoAuthResolver:
    def test_get_headers_raises_exception(self):
        no_auth_resolver = NoAuthResolver()
        with pytest.raises(NoAuthResolverException):
            no_auth_resolver.get_headers()

    def test_get_cookies_raises_exception(self):
        no_auth_resolver = NoAuthResolver()
        with pytest.raises(NoAuthResolverException):
            no_auth_resolver.get_cookies()

    def test_get_query_parameters_raises_exception(self):
        no_auth_resolver = NoAuthResolver()
        with pytest.raises(NoAuthResolverException):
            no_auth_resolver.get_query_parameters()


class TestAuthenticationResolverBase:
    def test_get_headers_returns_empty_dict(self):
        resolver = AuthenticationResolverBase()
        assert resolver.get_headers() == {}

    def test_get_cookies_returns_empty_dict(self):
        resolver = AuthenticationResolverBase()
        assert resolver.get_cookies() == {}

    def test_get_query_parameters_returns_empty_dict(self):
        resolver = AuthenticationResolverBase()
        assert resolver.get_query_parameters() == {}


class TestAuthenticationManager:
    def test_is_dependency_authenticated_returns_true_for_security_dependency(self):
        # given
        security_dependency = MagicMock()
        security_dependency.dependencies = [Security()]
        security_dependency.call = 'some_function'

        manager = AuthenticationManager([], MockAuthenticationResolver())
        # when - then
        assert manager.is_dependency_authenticated(security_dependency)

    def test_is_dependency_authenticated_returns_true_for_authenticated_dependencies(self):
        # given
        auth_dependency = MagicMock()
        auth_dependency.dependencies = []
        auth_dependency.call = 'some_function'

        manager = AuthenticationManager([auth_dependency.call], MockAuthenticationResolver())
        # when - then
        assert manager.is_dependency_authenticated(auth_dependency)

    def test_is_dependency_authenticated_returns_true_for_nested_authenticated_dependencies(self):
        # given
        auth_dependency = MagicMock()
        auth_dependency.dependencies = []
        auth_dependency.call = 'some_function'

        nested_dependency = MagicMock()
        nested_dependency.dependencies = [auth_dependency]
        nested_dependency.call = 'nested_function'

        manager = AuthenticationManager([auth_dependency.call], MockAuthenticationResolver())
        # when - then
        assert manager.is_dependency_authenticated(nested_dependency)

    def test_is_route_authenticated_returns_true_for_route_with_authenticated_dependency(self):
        # given
        route = MagicMock()
        route.dependencies = []
        route.dependant = MagicMock()
        route.dependant.dependencies = [Security()]
        route.dependant.call = 'some_function'

        manager = AuthenticationManager([], MockAuthenticationResolver())
        # when - then
        assert manager.is_route_authenticated(route)

    def test_is_route_authenticated_returns_true_for_route_with_authenticated_nested_dependency(self):
        # given
        auth_dependency = MagicMock()
        auth_dependency.dependencies = []
        auth_dependency.call = 'some_function'

        nested_dependency = MagicMock()
        nested_dependency.dependencies = [auth_dependency]
        nested_dependency.call = 'nested_function'

        route = MagicMock()
        route.dependencies = []
        route.dependant = nested_dependency

        manager = AuthenticationManager([auth_dependency.call], MockAuthenticationResolver())
        # when - then
        assert manager.is_route_authenticated(route)

    def test_is_route_authenticated_returns_false_for_route_with_unauthenticated_dependency(self):
        # given
        route = MagicMock()
        route.dependencies = []
        route.dependant = MagicMock()
        route.dependant.dependencies = []
        route.dependant.call = 'some_function'

        manager = AuthenticationManager([], NoAuthResolver())
        # when - then
        assert not manager.is_route_authenticated(route)

    def test_is_route_authenticated_returns_false_for_route_with_unauthenticated_nested_dependency(self):
        # given
        auth_dependency = MagicMock()
        auth_dependency.dependencies = []
        auth_dependency.call = 'some_function'

        nested_dependency = MagicMock()
        nested_dependency.dependencies = [auth_dependency]
        nested_dependency.call = 'nested_function'

        route = MagicMock()
        route.dependencies = []
        route.dependant = nested_dependency

        manager = AuthenticationManager([], NoAuthResolver())
        # when - then
        assert not manager.is_route_authenticated(route)

    def test_headers_returns_correct_headers(self):
        # given
        resolver = MockAuthenticationResolver()
        manager = AuthenticationManager([], resolver)
        # when - then
        assert manager.headers == {'Authorization': 'Bearer my_token'}

    def test_cookies_returns_correct_cookies(self):
        # given
        resolver = MockAuthenticationResolver()
        manager = AuthenticationManager([], resolver)
        # when - then
        assert manager.cookies == {'session_id': 'my_session_id'}

    def test_query_parameters_returns_correct_query_parameters(self):
        # given
        resolver = MockAuthenticationResolver()
        manager = AuthenticationManager([], resolver)
        # when - then
        assert manager.query_parameters == {'api_key': 'my_api_key'}

    def test_set_server_url_should_call_authentication_resolver_server_url(self):
        # given
        resolver = MockAuthenticationResolver()
        manager = AuthenticationManager([], resolver)
        # when
        manager.set_server_url("https://127.0.0.1:7465")
        # then
        assert resolver.server_url == "https://127.0.0.1:7465"
