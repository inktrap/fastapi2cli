import pytest

from fastapi2cli.parsers import parse_url, make_parse_literal


def test_parse_url_with_valid_scheme_should_return_url():
    # given
    url = "https://example.com/"
    valid_schemes = ["https"]
    # when
    parsed_url = parse_url(url, valid_schemes)
    # then
    assert parsed_url == url


def test_parse_url_with_invalid_scheme_should_raise_value_error():
    # given
    url = "ftp://example.com/"
    valid_schemes = ["http", "https"]
    # when / then
    with pytest.raises(ValueError):
        parse_url(url, valid_schemes)


def test_parse_url_with_default_schemes_should_return_url():
    # given
    url = "http://example.com/"
    # when
    parsed_url = parse_url(url)
    # then
    assert parsed_url == url


def test_make_parse_literal_with_valid_value_should_return_value():
    # given
    literals = ["a", "b", "c"]
    parse_literal_func = make_parse_literal(literals)
    # when
    result = parse_literal_func("b")
    # then
    assert result == "b"


def test_make_parse_literal_with_invalid_value_should_raise():
    # given
    literals = ["a", "b", "c"]
    parse_literal_func = make_parse_literal(literals)
    # when / then
    with pytest.raises(ValueError):
        parse_literal_func("d")
