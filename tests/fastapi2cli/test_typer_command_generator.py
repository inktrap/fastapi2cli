import inspect
import json
from datetime import datetime, timedelta
from enum import Enum
from typing import List, Optional, Literal, Dict, Annotated, Union

import pytest
from pydantic import BaseModel
from pydantic.fields import FieldInfo
from typer.models import OptionInfo, ArgumentInfo

from fastapi2cli.parsers import parse_url
from fastapi2cli.typer_command_generator import get_inspect_signature, TyperCommandGenerator, TYPER_SUPPORTED_TYPES
from tests.matchers import InstanceOfWith

MODULE_PACKAGE = "fastapi2cli.typer_command_generator"


class CustomEnum(Enum):
    VALUE1 = "value1"
    VALUE2 = "value2"


def sample_function():
    pass


@pytest.fixture
def typer_command_generator():
    return TyperCommandGenerator({})


def test_get_inspect_signature():
    annotations = {
        "a": int,
        "b": str,
        "c": Optional[float],
        "d": datetime,
        "e": CustomEnum
    }
    defaults = {
        "c": None,
        "d": datetime.now(),
        "e": CustomEnum.VALUE1
    }
    # when
    signature = get_inspect_signature(sample_function, annotations, defaults)
    # then
    assert len(signature.parameters) == 5
    assert signature.parameters["a"] == inspect.Parameter("a", inspect.Parameter.KEYWORD_ONLY, annotation=int)
    assert signature.parameters["b"] == inspect.Parameter("b", inspect.Parameter.KEYWORD_ONLY, annotation=str)
    assert signature.parameters["c"] == inspect.Parameter("c", inspect.Parameter.KEYWORD_ONLY,
                                                          annotation=Optional[float], default=None)
    assert signature.parameters["d"] == inspect.Parameter("d", inspect.Parameter.KEYWORD_ONLY, annotation=datetime,
                                                          default=defaults["d"])
    assert signature.parameters["e"] == inspect.Parameter("e", inspect.Parameter.KEYWORD_ONLY, annotation=CustomEnum,
                                                          default=CustomEnum.VALUE1)


def test_get_inspect_signature_no_defaults():
    annotations = {
        "a": int,
        "b": str
    }
    # when
    signature = get_inspect_signature(sample_function, annotations, {})
    # then
    assert len(signature.parameters) == 2
    assert signature.parameters["a"] == inspect.Parameter("a", inspect.Parameter.KEYWORD_ONLY, annotation=int)
    assert signature.parameters["b"] == inspect.Parameter("b", inspect.Parameter.KEYWORD_ONLY, annotation=str)


def test_get_inspect_signature_empty_annotations():
    signature = get_inspect_signature(sample_function, {}, {})
    assert str(signature) == "()"


def test_get_inspect_signature_optional_enum():
    annotations = {
        "a": int,
        "b": str,
        "c": Optional[CustomEnum]
    }
    defaults = {
        "c": None
    }
    # when
    signature = get_inspect_signature(sample_function, annotations, defaults)
    # then
    assert len(signature.parameters) == 3
    assert signature.parameters["a"] == inspect.Parameter("a", inspect.Parameter.KEYWORD_ONLY, annotation=int)
    assert signature.parameters["b"] == inspect.Parameter("b", inspect.Parameter.KEYWORD_ONLY, annotation=str)
    assert signature.parameters["c"] == inspect.Parameter("c", inspect.Parameter.KEYWORD_ONLY,
                                                          annotation=Optional[CustomEnum], default=None)


def test_typer_command_generator_get_parser_for_complex_type_should_return_parser_for_union(typer_command_generator,
                                                                                            mocker):
    # given
    get_optional_type_value_mock = mocker.patch(f"{MODULE_PACKAGE}.get_optional_type_value")
    get_parser_for_type_patch = mocker.patch.object(typer_command_generator, "get_parser_for_type")

    expected_parser = get_parser_for_type_patch.return_value
    # when
    parser = typer_command_generator.get_parser_for_complex_type(Union[str, None])
    # then
    assert parser == expected_parser
    get_parser_for_type_patch.assert_called_once_with(get_optional_type_value_mock.return_value)
    get_optional_type_value_mock.assert_called_once_with(Union[str, None])


@pytest.mark.parametrize("type_",
                         [list, List] +
                         [list[type_] for type_ in TYPER_SUPPORTED_TYPES] +
                         [List[type_] for type_ in TYPER_SUPPORTED_TYPES]
                         )
def test_typer_command_generator_get_parser_for_complex_type_should_return_none_for_list_of_supported_type(type_,
                                                                                                           typer_command_generator):
    # given

    # when
    parser = typer_command_generator.get_parser_for_complex_type(type_)
    # then
    assert parser is None


@pytest.mark.parametrize("dict_type", [dict, Dict, dict[str, int], Dict[str, bool]])
def test_typer_command_generator_get_parser_for_complex_type_should_return_parser_for_dict_with_params(dict_type,
                                                                                                       typer_command_generator):
    # given
    expected_parser = json.loads
    # when
    parser = typer_command_generator.get_parser_for_complex_type(dict_type)
    # then
    assert parser == expected_parser


def test_typer_command_generator_get_parser_for_complex_type_should_return_parser_for_literal(typer_command_generator,
                                                                                              mocker):
    # given
    make_parse_literal_mock = mocker.patch(f"{MODULE_PACKAGE}.make_parse_literal")
    expected_parser = make_parse_literal_mock.return_value
    # when
    parser = typer_command_generator.get_parser_for_complex_type(Literal["a", "b"])
    # then
    assert parser == expected_parser
    make_parse_literal_mock.assert_called_once_with(("a", "b"))


def test_typer_command_generator_get_parser_for_complex_type_should_raise_error_for_unsupported_type(
        typer_command_generator):
    # given
    # then
    with pytest.raises(TypeError):
        # when
        typer_command_generator.get_parser_for_complex_type(int)


@pytest.mark.parametrize("type_", TYPER_SUPPORTED_TYPES)
def test_typer_command_generator_get_parser_for_type_should_return_none_for_supported_typer_type(type_):
    # given
    generator = TyperCommandGenerator({})
    # when
    parser = generator.get_parser_for_type(type_)
    # then
    assert parser is None


@pytest.mark.parametrize("type_", TyperCommandGenerator.parsers)
def test_typer_command_generator_get_parser_for_type_should_return_default_parser_for_provided_parser(type_):
    # given
    generator = TyperCommandGenerator({})
    # when
    parser = generator.get_parser_for_type(type_)
    # then
    assert parser is TyperCommandGenerator.parsers[type_]


def test_typer_command_generator_get_parser_for_type_should_return_none_for_enum():
    # given
    generator = TyperCommandGenerator({})
    # when
    parser = generator.get_parser_for_type(CustomEnum)
    # then
    assert parser is None


def test_typer_command_generator_get_parser_for_type_should_return_model_json_validate(typer_command_generator):
    # given
    class MyModel(BaseModel):
        name: str

    # when
    parser = typer_command_generator.get_parser_for_type(MyModel)
    # then
    assert parser == MyModel.model_validate_json


def test_typer_command_generator_get_parser_for_type_should_return_custom_parser_from_user():
    # given
    def fun(value: str):
        pass

    generator = TyperCommandGenerator({timedelta: fun})
    # when
    parser = generator.get_parser_for_type(timedelta)
    # then
    assert parser is fun


def test_typer_command_generator_get_parser_for_type_should_call_get_parser_from_complex_type(mocker):
    # given
    generator = TyperCommandGenerator()
    get_parser_for_complex_type_patch = mocker.patch.object(generator, "get_parser_for_complex_type")
    # when
    parser = generator.get_parser_for_type(list[str])
    # then
    assert parser == get_parser_for_complex_type_patch.return_value


def test_typer_command_generator_get_proxy_for_parameter_should_return_argument(typer_command_generator, mocker):
    # given
    get_parser_for_type_patch = mocker.patch.object(typer_command_generator, "get_parser_for_type")
    field_info = FieldInfo(annotation=str, description="Test parameter")
    # when
    proxy = typer_command_generator.get_proxy_for_parameter(field_info)
    # then
    assert isinstance(proxy, ArgumentInfo)
    assert proxy.help == "Test parameter"
    assert proxy.parser == get_parser_for_type_patch.return_value


def test_typer_command_generator_get_proxy_for_parameter_should_return_option_info(typer_command_generator, mocker):
    # given
    get_parser_for_type_patch = mocker.patch.object(typer_command_generator, "get_parser_for_type")
    field_info = FieldInfo(annotation=str, description="Test parameter", default=None)
    # when
    proxy = typer_command_generator.get_proxy_for_parameter(field_info)
    # then
    assert isinstance(proxy, OptionInfo)
    assert proxy.help == "Test parameter"
    assert proxy.parser == get_parser_for_type_patch.return_value


def test_set_function_annotations_for_route_with_server_url(typer_command_generator):
    # given
    parameters = {
        "a": FieldInfo(annotation=int, description="Parameter A"),
        "b": FieldInfo(annotation=str, description="Parameter B", default=None),
    }
    function = lambda a: None
    server_url = "https://example.com"
    # when
    typer_command_generator.set_function_annotations_for_route(function, parameters, server_url)
    # then
    assert len(function.__annotations__) == 3
    assert function.__annotations__["a"] == Annotated[int, InstanceOfWith(ArgumentInfo, help="Parameter A")]
    assert function.__annotations__["b"] == Annotated[str, InstanceOfWith(OptionInfo, help="Parameter B")]
    assert function.__annotations__["server_url"] == Annotated[
        str, InstanceOfWith(OptionInfo, help="the server base url (e.g. https://127.0.0.1:443)", parser=parse_url)]
    assert len(function.__signature__.parameters) == 3
    assert function.__signature__.parameters["b"].default is None
    assert len(function.__kwdefaults__) == 2
    assert function.__kwdefaults__ == {"b": None, "server_url": server_url}


def test_set_function_annotations_for_route_without_server_url(typer_command_generator):
    # given
    parameters = {
        "a": FieldInfo(annotation=int, description="Parameter A"),
        "b": FieldInfo(annotation=str, description="Parameter B", default=None),
    }
    function = lambda a: None
    # when
    typer_command_generator.set_function_annotations_for_route(function, parameters)
    # then
    assert len(function.__annotations__) == 3
    assert function.__annotations__["a"] == Annotated[int, InstanceOfWith(ArgumentInfo, help="Parameter A")]
    assert function.__annotations__["b"] == Annotated[str, InstanceOfWith(OptionInfo, help="Parameter B")]
    assert function.__annotations__["server_url"] == Annotated[
        str, InstanceOfWith(OptionInfo, help="the server base url (e.g. https://127.0.0.1:443)", parser=parse_url)]
    assert len(function.__signature__.parameters) == 3
    assert function.__signature__.parameters["b"].default is None
    assert len(function.__kwdefaults__) == 1
    assert function.__kwdefaults__ == {"b": None}


def test_set_function_annotations_for_route_with_child_param(typer_command_generator):
    # given
    parameters = {
        "a": FieldInfo(annotation=int, description="Parameter A"),
        "-host_b": FieldInfo(annotation=str, description="Parameter B", default=None),
        "-host_c": FieldInfo(annotation=bool, description="Parameter C"),
    }
    function = lambda a: None
    # when
    typer_command_generator.set_function_annotations_for_route(function, parameters)
    # then
    assert len(function.__annotations__) == 4
    assert function.__annotations__["a"] == Annotated[int, InstanceOfWith(ArgumentInfo, help="Parameter A")]
    assert function.__annotations__["host_b"] == Annotated[str, InstanceOfWith(OptionInfo, help="Parameter B")]
    assert function.__annotations__["host_c"] == Annotated[bool, InstanceOfWith(OptionInfo, help="Parameter C")]
    assert function.__annotations__["server_url"] == Annotated[
        str, InstanceOfWith(OptionInfo, help="the server base url (e.g. https://127.0.0.1:443)", parser=parse_url)]
    assert len(function.__signature__.parameters) == 4
    assert function.__signature__.parameters["host_b"].default is None
    assert len(function.__kwdefaults__) == 1
    assert function.__kwdefaults__ == {"host_b": None}


def test_set_function_annotations_for_route_with_provided_extra_args(typer_command_generator):
    # given
    parameters = {
        "a": FieldInfo(annotation=int, description="Parameter A"),
        "z": FieldInfo(annotation=str, description="Parameter Z"),
    }
    function = lambda a: None
    # when
    typer_command_generator.set_function_annotations_for_route(function, parameters, z="world")
    # then
    assert len(function.__annotations__) == 3
    assert function.__annotations__["a"] == Annotated[int, InstanceOfWith(ArgumentInfo, help="Parameter A")]
    assert function.__annotations__["z"] == Annotated[str, InstanceOfWith(ArgumentInfo, help="Parameter Z")]
    assert function.__annotations__["server_url"] == Annotated[
        str, InstanceOfWith(OptionInfo, help="the server base url (e.g. https://127.0.0.1:443)", parser=parse_url)]
    assert len(function.__signature__.parameters) == 3
    assert function.__signature__.parameters["z"].default == "world"
    assert len(function.__kwdefaults__) == 1
    assert function.__kwdefaults__ == {"z": "world"}

def test_set_function_annotations_for_route_with_provided_extra_args_not_in_parameters(typer_command_generator):
    # given
    parameters = {
        "a": FieldInfo(annotation=int, description="Parameter A"),
        "z": FieldInfo(annotation=str, description="Parameter Z"),
    }
    function = lambda a: None
    # when
    typer_command_generator.set_function_annotations_for_route(function, parameters, w="world")
    # then
    assert len(function.__annotations__) == 3
    assert function.__annotations__["a"] == Annotated[int, InstanceOfWith(ArgumentInfo, help="Parameter A")]
    assert function.__annotations__["z"] == Annotated[str, InstanceOfWith(ArgumentInfo, help="Parameter Z")]
    assert function.__annotations__["server_url"] == Annotated[
        str, InstanceOfWith(OptionInfo, help="the server base url (e.g. https://127.0.0.1:443)", parser=parse_url)]
    assert len(function.__signature__.parameters) == 3
    assert len(function.__kwdefaults__) == 0
