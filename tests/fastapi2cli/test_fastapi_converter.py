import types

import pytest
from fastapi import FastAPI
from fastapi.routing import APIRoute
from typer import Typer

from fastapi2cli.fastapi_converter import FastAPIConverter
from fastapi2cli.typer_command_generator import TyperCommandGenerator
from fastapi2cli.web.server_interactor import ServerInteractor
from tests.matchers import Any_, InstanceOfWith

MODULE_PACKAGE = "fastapi2cli.fastapi_converter"


@pytest.fixture()
def fastapi_converter(mocker):
    return FastAPIConverter(
        mocker.Mock(return_value=mocker.Mock(spec=ServerInteractor)),
        mocker.Mock(spec=TyperCommandGenerator)
    )


def test_generate_function_for_route_should_return_a_typed_function(fastapi_converter, mocker):
    # given
    get_parameters_for_route_patch = mocker.patch(f"{MODULE_PACKAGE}.get_parameters_for_route")
    convert_parameters_to_typer_supported_format_patch = mocker.patch(
        f"{MODULE_PACKAGE}.convert_parameters_to_typer_supported_format")
    route = APIRoute("/", lambda: None)
    # when
    result = fastapi_converter.generate_function_for_route(route)
    # then
    assert isinstance(result, types.FunctionType)
    get_parameters_for_route_patch.assert_called_once_with(route)
    convert_parameters_to_typer_supported_format_patch.assert_called_once_with(
        get_parameters_for_route_patch.return_value)
    fastapi_converter.typer_command_generator.set_function_annotations_for_route.assert_called_once_with(
        result,
        convert_parameters_to_typer_supported_format_patch.return_value,
        None
    )


def test_expose_route_in_typer_cli_should_register_route_caller_into_typer_app(fastapi_converter, mocker):
    # given
    def fun(server_url: str):
        pass

    def endpoint():
        pass

    generate_function_for_route_patch = mocker.patch.object(fastapi_converter, "generate_function_for_route",
                                                            return_value=fun)
    route = APIRoute("/", endpoint)
    app = mocker.Mock(spec=Typer)
    # when
    fastapi_converter.expose_route_in_typer_cli(route, app)
    # then
    generate_function_for_route_patch.assert_called_once_with(route)
    app.command.assert_called_once_with(name="endpoint", help="", deprecated=None)
    app.command.return_value.assert_called_once_with(fun)


def test_expose_route_in_typer_cli_should_register_route_caller_into_typer_app_deprecated_and_description(
        fastapi_converter, mocker):
    # given
    def fun(server_url: str):
        pass

    def endpoint():
        """
        an endpoint description
        """
        pass

    generate_function_for_route_patch = mocker.patch.object(fastapi_converter, "generate_function_for_route",
                                                            return_value=fun)
    route = APIRoute("/", endpoint, deprecated=True)
    app = mocker.Mock(spec=Typer)
    # when
    fastapi_converter.expose_route_in_typer_cli(route, app)
    # then
    generate_function_for_route_patch.assert_called_once_with(route)
    app.command.assert_called_once_with(name="endpoint", help="an endpoint description", deprecated=True)
    app.command.return_value.assert_called_once_with(fun)


def test_expose_route_in_typer_cli_should_register_route_caller_into_typer_app_with_no_args_is_help(fastapi_converter,
                                                                                                    mocker):
    # given
    def fun(server_url: str, name: str):
        pass

    def endpoint(name: str):
        pass

    generate_function_for_route_patch = mocker.patch.object(fastapi_converter, "generate_function_for_route",
                                                            return_value=fun)
    route = APIRoute("/", endpoint)
    app = mocker.Mock(spec=Typer)
    # when
    fastapi_converter.expose_route_in_typer_cli(route, app)
    # then
    generate_function_for_route_patch.assert_called_once_with(route)
    app.command.assert_called_once_with(name="endpoint", help="", deprecated=None, no_args_is_help=True)
    app.command.return_value.assert_called_once_with(fun)


def test_expose_route_in_typer_cli_should_register_route_caller_into_typer_app_with_help_panel(fastapi_converter,
                                                                                               mocker):
    # given
    def fun(server_url: str):
        pass

    def endpoint():
        pass

    generate_function_for_route_patch = mocker.patch.object(fastapi_converter, "generate_function_for_route",
                                                            return_value=fun)
    route = APIRoute("/", endpoint, tags=["user"])
    app = mocker.Mock(spec=Typer)
    # when
    fastapi_converter.expose_route_in_typer_cli(route, app)
    # then
    generate_function_for_route_patch.assert_called_once_with(route)
    app.command.assert_called_once_with(name="endpoint", help="", deprecated=None, rich_help_panel="user")
    app.command.return_value.assert_called_once_with(fun)


def test_expose_app_routes_in_typer_cli_should_expose_all_routes(fastapi_converter, mocker):
    # given
    app = FastAPI()

    @app.get("/")
    def get():
        pass

    @app.get("/error")
    def error():
        pass

    @app.post("/create")
    def post():
        pass

    expose_route_in_typer_cli_patch = mocker.patch.object(fastapi_converter, "expose_route_in_typer_cli",
                                                          side_effect=[None, ValueError, None])
    # when
    fastapi_converter.expose_app_routes_in_typer_cli(app)
    # then
    assert expose_route_in_typer_cli_patch.call_count == 3
    expose_route_in_typer_cli_patch.assert_any_call(InstanceOfWith(APIRoute, endpoint=get), Any_(Typer))
    expose_route_in_typer_cli_patch.assert_any_call(InstanceOfWith(APIRoute, endpoint=error), Any_(Typer))
    expose_route_in_typer_cli_patch.assert_any_call(InstanceOfWith(APIRoute, endpoint=post), Any_(Typer))


def test_generate_function_for_route_request_should_send_request_to_server(fastapi_converter, mocker):
    # Given
    route = APIRoute("/", lambda: None)
    server_url = "http://example.com"
    set_function_annotations_for_route_patch = mocker.patch.object(
        fastapi_converter.typer_command_generator, "set_function_annotations_for_route"
    )

    # When
    request_function = fastapi_converter.generate_function_for_route(route)
    response_mock = mocker.Mock()
    fastapi_converter.server_interactor_factory.return_value.request.return_value = response_mock
    result = request_function(server_url, param1="value1", param2="value2")

    # Then
    fastapi_converter.server_interactor_factory.assert_called_once_with(server_url)
    fastapi_converter.server_interactor_factory.return_value.request.assert_called_once_with(route, param1="value1",
                                                                                             param2="value2")
    set_function_annotations_for_route_patch.assert_called_once()
    assert result == response_mock
