from datetime import timedelta

import httpx
import pytest
from fastapi import FastAPI
from typer import Typer

from fastapi2cli import expose_app
from fastapi2cli.fastapi_converter import FastAPIConverter
from fastapi2cli.typer_command_generator import TyperCommandGenerator
from fastapi2cli.web.authentication import AuthenticationManager, NoAuthResolver, AuthenticationResolverBase
from fastapi2cli.web.server_interactor import ServerInteractor, RequestExecutor
from tests.matchers import Any_

MODULE_PACKAGE = "fastapi2cli.expose"


@pytest.fixture()
def server_interactor_factory_patch(mocker):
    return mocker.patch(f'{MODULE_PACKAGE}.partial')


@pytest.fixture()
def fastapi_converter_patch(mocker):
    return mocker.patch(f'{MODULE_PACKAGE}.FastAPIConverter', return_value=mocker.Mock(spec=FastAPIConverter))


@pytest.fixture()
def typer_command_generator_patch(mocker):
    return mocker.patch(f'{MODULE_PACKAGE}.TyperCommandGenerator', return_value=mocker.Mock(spec=TyperCommandGenerator))


@pytest.fixture()
def authentication_manager_patch(mocker):
    return mocker.patch(f'{MODULE_PACKAGE}.AuthenticationManager', return_value=mocker.Mock(spec=AuthenticationManager))


@pytest.fixture()
def app():
    return FastAPI()


def test_expose_app_should_call_expected_methods(app,
                                                 typer_command_generator_patch, fastapi_converter_patch,
                                                 server_interactor_factory_patch, authentication_manager_patch):
    # Given
    fastapi_converter_patch.return_value.expose_app_routes_in_typer_cli.return_value = Typer()

    # When
    expose_app(app)

    # Then
    authentication_manager_patch.assert_called_once_with([], Any_(NoAuthResolver))
    server_interactor_factory_patch.assert_called_once_with(
        ServerInteractor,
        request_executor=httpx,
        authentication_manager=authentication_manager_patch.return_value
    )
    fastapi_converter_patch.assert_called_once_with(
        server_interactor_factory_patch.return_value,
        typer_command_generator_patch.return_value,
        None
    )
    typer_command_generator_patch.assert_called_once_with({})
    fastapi_converter_patch.return_value.expose_app_routes_in_typer_cli.assert_called_once_with(app)


def test_expose_app_should_use_defaults_for_optional_params(mocker, app,
                                                            typer_command_generator_patch, fastapi_converter_patch,
                                                            server_interactor_factory_patch,
                                                            authentication_manager_patch):
    # Given
    fastapi_converter_patch.return_value.expose_app_routes_in_typer_cli.return_value = Typer()

    authentication_resolver = mocker.Mock(spec=AuthenticationResolverBase)
    authenticated_dependencies = [lambda: None]
    request_executor = mocker.Mock(spec=RequestExecutor)
    additional_parsers = {timedelta: lambda value: None}
    server_url = "https://127.0.0.1"
    # When
    expose_app(app,
               authentication_resolver=authentication_resolver,
               authenticated_dependencies=authenticated_dependencies,
               request_executor=request_executor,
               additional_parsers=additional_parsers,
               server_url=server_url)

    # Then
    authentication_manager_patch.assert_called_once_with(authenticated_dependencies, authentication_resolver)
    server_interactor_factory_patch.assert_called_once_with(
        ServerInteractor,
        request_executor=request_executor,
        authentication_manager=authentication_manager_patch.return_value
    )
    fastapi_converter_patch.assert_called_once_with(
        server_interactor_factory_patch.return_value,
        typer_command_generator_patch.return_value,
        server_url
    )
    typer_command_generator_patch.assert_called_once_with(additional_parsers)
    fastapi_converter_patch.return_value.expose_app_routes_in_typer_cli.assert_called_once_with(app)
