from typing import Annotated
from uuid import UUID

import pytest
from fastapi import Depends, Security
from fastapi._compat import ModelField
from fastapi.dependencies.models import Dependant
from fastapi.params import Query, Path
from fastapi.routing import APIRoute
from fastapi.security import SecurityScopes, OAuth2PasswordBearer
from pydantic.fields import FieldInfo

from fastapi2cli.parameters.get_params_for import get_parameters_for_dependency, get_parameters_for_route
from tests.matchers import InstanceOfWith


def test_get_parameters_for_dependency_returns_empty_dict_for_dependency_without_parameters():
    # given
    dependency = Dependant()
    # when
    result = get_parameters_for_dependency(dependency)
    # then
    assert result == {}


@pytest.mark.parametrize("dependency_location",
                         ["path_params", "query_params", "header_params", "cookie_params", "body_params"])
def test_get_parameters_for_dependency_returns_own_parameters(dependency_location):
    # given
    field_info = FieldInfo(annotation=str)
    dependency = Dependant(
        **{dependency_location: [ModelField(field_info, "customer_name")]}
    )
    # when
    result = get_parameters_for_dependency(dependency)
    # then
    assert result == {"customer_name": field_info}


def test_get_parameters_for_dependency_returns_empty_dict_if_path_params_exclude():
    # given
    field_info = FieldInfo(annotation=str)
    dependency = Dependant(
        path_params=[ModelField(field_info, "customer_name")]
    )
    # when
    result = get_parameters_for_dependency(dependency,
                                           ["query_params", "header_params", "cookie_params", "body_params"])
    # then
    assert result == {}


@pytest.mark.parametrize("dependency_location",
                         ["path_params", "query_params", "header_params", "cookie_params", "body_params"])
def test_get_parameters_for_dependency_returns_child_dependency_parameters(dependency_location):
    # given
    field_info = FieldInfo(annotation=str)
    child_dependency = Dependant(
        **{dependency_location: [ModelField(field_info, "customer_name")]}
    )
    dependency = Dependant(dependencies=[child_dependency])
    # when
    result = get_parameters_for_dependency(dependency)
    # then
    assert result == {"customer_name": field_info}


@pytest.mark.parametrize("dependency_location",
                         ["path_params", "query_params", "header_params", "cookie_params", "body_params"])
def test_get_parameters_for_dependency_returns_deep_link_child_dependency_parameters(dependency_location):
    # given
    field_info = FieldInfo(annotation=str)
    child_dependency = Dependant(
        **{dependency_location: [ModelField(field_info, "customer_name")]}
    )
    dependency2 = Dependant(dependencies=[child_dependency])
    dependency3 = Dependant(dependencies=[dependency2])
    other_dependency = Dependant()
    dependency4 = Dependant(dependencies=[dependency3, other_dependency])
    dependency = Dependant(dependencies=[dependency4])
    # when
    result = get_parameters_for_dependency(dependency)
    # then
    assert result == {"customer_name": field_info}


def test_get_parameters_for_route_should_get_parameters_for_route_dependant():
    def fun(name: str):
        pass

    route = APIRoute("/", fun)
    # when
    result = get_parameters_for_route(route)
    # then
    assert result == {"name": InstanceOfWith(Query, annotation=str)}


def test_get_parameters_for_route_should_get_parameters_for_route_dependant_path():
    def fun(name: str):
        pass

    route = APIRoute("/{name}", fun)
    # when
    result = get_parameters_for_route(route)
    # then
    assert result == {"name": InstanceOfWith(Path, annotation=str)}


def test_get_parameters_for_route_should_get_parameters_for_route_child_dependency():
    def get_host_service(project_id: UUID):
        pass

    def fun(service: Annotated[str, Depends(get_host_service)]):
        pass

    route = APIRoute("/", fun)
    # when
    result = get_parameters_for_route(route)
    # then
    assert result == {"project_id": InstanceOfWith(Query, annotation=UUID)}


def test_get_parameters_for_route_should_ignore_security_dependency():
    oauth2_scheme = OAuth2PasswordBearer(
        tokenUrl="token",
        scopes={"read": "read"}
    )

    def get_current_active_user(security_scopes: SecurityScopes, token: Annotated[str, Depends(oauth2_scheme)]):
        pass

    def fun(project_id: UUID):
        pass

    route = APIRoute("/", fun, dependencies=[Security(get_current_active_user)])
    # when
    result = get_parameters_for_route(route)
    # then
    assert result == {"project_id": InstanceOfWith(Query, annotation=UUID)}

