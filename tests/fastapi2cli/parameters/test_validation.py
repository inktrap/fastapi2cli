import pytest
from pydantic.fields import FieldInfo

from fastapi2cli.parameters.validation import validate_parameter_consistency


def test_validate_parameter_consistency_raises_error_on_conflict():
    # given
    existing_parameters = {"param1": FieldInfo(annotation=str)}
    new_parameter_name = "param1"
    new_parameter_value = FieldInfo(annotation=int)
    # then
    with pytest.raises(ValueError):
        # when
        validate_parameter_consistency(new_parameter_name, new_parameter_value, existing_parameters)


def test_validate_parameter_consistency_does_not_raise_error_on_consistency():
    # given
    existing_parameters = {"param1": FieldInfo(annotation=str)}
    new_parameter_name = "param2"
    new_parameter_value = FieldInfo(annotation=int)
    # when
    validate_parameter_consistency(new_parameter_name, new_parameter_value, existing_parameters)
    # then no error should be raised


def test_validate_parameter_consistency_does_not_raise_error_if_param_name_not_in_existing_params():
    # given
    existing_parameters = {}
    new_parameter_name = "param2"
    new_parameter_value = FieldInfo(annotation=int)
    # when
    validate_parameter_consistency(new_parameter_name, new_parameter_value, existing_parameters)
    # then no error should be raised
